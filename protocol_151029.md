Sitzungsprotokoll
=====
vom 29.10.15 20:00 - 22:15 Hotel Stadt Wien, Bad Schallerbach


# Anwesenheit
## Anwesende Personen
- Thomas Teubel
- Tim Teubel
- Christine Gruber (entschuldigt verspätet)
- Raimund Buchegger
- Alois Leitner
- Christina Sporn

## Abwesende Personen
- Markus Ortbauer

# Themen

## Anträge
### Angenommene Anträge
- Anfallende Kosten bei Sitzungen werden aus der Parteikassa bezahlt

### Abgelehnte Anträge
- Keine

## Mitgliederämter

### Fraktionsobmann
 - Thomas Teubel
### Sprecher (+Stellvertreter)
 - Thomas Teubel (Tim Teubel)
### Schatzmeister/Finanzreferent (+Prüferin)
 - Raimund Buchegger (Christina Sporn)

## Ausschüsse
### Fraktionsmitglieder + Mitglieder mit beratender Stimme
#### Prüfungsausschuss
- Thomas Teubel (Vertretung: Tim Teubel)
#### Straßen- & Bauangelegenheiten, Wirtschaft, Tourismus, Sport
- Tim Teubel (Vertretung: Thomas Teubel)
#### Örtliche Umweltfragen und Gesundheit
- Raimund Buchegger (Vertretung: Christine Gruber)
#### Familie, Kindergarten, Schule, Jungend, Integration
- Christine Gruber (Vertretung: Christina Sporn)

### Tagesordnungen 
 - Tagesordnungen werden (meist relativ knapp, min. aber 1 Woche vorher) vorher bekannt gegeben
 - VORSICHT: Je höher das Interesse, desto knapper die Termine -> richtiges Vorbereiten,...
 
 - Bei Verhinderung in Ausschüssen muss die Gemeinde informiert werden, die anschließend die Vertretung informiert!

WICHTIG: Gemeinderatssitzungen dürfen aufgezeichnet werden!

# Allgemeine Informationen
- Bei Rechtsangelegenheiten steht 1 1/2 h im Jahr der Rechtsanwalt Staudinger zur Verfügung
- "Grüne Bildungswerkstatt" -> Homepage anschauen, etc.
  Wenn es Interessen gibt -> Antrag stellen -> Raimund
- Statt mehrmaliger Zeitung im Jahr -> Newsletter
  Information zu Newslettersystem bei Grünen
- Homepage: Zugriffsstatistiken

## Finanzen

### Schatzmeister-Abstimmung
 - Ein Schatzmeister: Raimund Buchegger (einstimmig)
 - Ein Prüfer: Christina Sporn (einstimmig)
   (Muss nach Linz übermittelt werden)
### Transparenzerklärung
 

## Anträge
- Anträge an Thomas für Prüfungsausschuss
 - Bei Anträgen immer mit "Fakten" bzw. belegbare Dingen, etc.
  Kein Fernbleiben ohne Meldung!
- Zu etwaigen Anliegen müssen VOR Sitzungen Anträge gestellt werden

# Aufgaben
- Mitgliederämter (Schatzmeister, etc) an Linz übermitteln
- Transparanzerklärung nach Linz übermitteln
- 6 Exemplare der "Oberösterreichischen Gemeindeordnung" bestellen
- Antrag: Termine der Gemeinderatssitzung in die Gemeindezeitung, bzw. immer den nächsten Termin
- Termin für Ausflug / Ausflugsziel (eventuell Waldviertel)
- Aktuelle Liste der Mitglieder (Name, Telefonnummer, Adresse) erstellen!
- Bilder auf Homepage wechseln

# Nächsten Termine
- Weihnachten? 
- Ort: Gartenhaus beim Lois






