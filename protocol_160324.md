Sitzungsprotokoll
=====
vom 24.03.16 19:00 - 22:15 Grünen Zentrale, Gartenhaus


# Anwesenheit
## Anwesende Personen
- Thomas Teubel
- Tim Teubel
- Christine Gruber (entschuldigt verspätet)
- Raimund Buchegger (entschuldigt verspätet)
- Alois Leitner

## Abwesende Personen
- Christina Sporn

# Themen

## Anträge
### Angenommene Anträge
- 250€ für Sponsoring des Juniorscup SV sedda Bad Schallerbach
-- Großes Logo auf Seite 1 + Postwurfsendung an alle Haushalte im Ort

### Abgelehnte Anträge
- Keine


# Allgemeine Informationen
- 05.April Bezirksvorstandssitzung
-- Thomas und Tim werden der Sitzung beiwohnen
- Unterstützungserklärungen Van der Bellen
-- Thomas hat 30 Stück nachbeantragt -> sind nicht angekommen -> Investigieren was da los ist!
- Plakte!
-- Raimund lässt zwei kleine Plakate aufhängen
-- Der Rest wird "vom Land" angebracht
- Mitteilen, falls wir Aktionen oder Infostände machen
- Nach Ostern können Bleistifte + Flyer bestellt werden -> Tim kümmert sich darum
- Freitag, 06. Mai ist Fahrzeugweihung der Feuerwehr
-- Thomas und Christine sind fix dort


## Finanzen
- Kassier (Raimund) Entlastung
-- Rechnungsabschluss wurde an Linz übermittelt
-- Nachfrage bzgl. 90€ Spende wurde geklärt
-- Kontestand: [redacted] (2014) -> [redacted] Sparbrief Einlage
-- Kontestand: [redacted] per 31.12.2015
- Überlegung, ob man weitere Mittel auch an nicht so "erfolgreiche" Grünen-Gruppen weitergeben kann
- Klärung Sitzungsgeld
-- Wie bisher, zur freien Verfügung (darf einbehalten werden)!
- Thomas legt [redacted] (von [redacted]) pro Monat aufs Konto zurück 


# Aufgaben
- Evaluieren ob Infostände bzw. Aktionen bzgl. Bundespräsidentenwahl geplant werden sollen
- Homepagesteckzettel ausfüllen und an Alois weiterleiten
- Flüchtlichngsprojekte: Malprojekt, Fußballinformationsprojekt
-- Kleine Beschreibung der Aktionen für Homepage
- Wir wollen mal ein "Abschluss"-Essen mit "ehemaligen" Mitgliedern bzw. Engagierte
-- Raimund Vorschlag: Pepe Nero?
-- Terminvorschlag: Anfang Mai
-- Einladenungen z.B. an Markus Ortbauer
- Eventuell die Treffen vorverschieben -> 18:30
-- Alois soll das Erinnerungsmail konsequenter und früher ausschicken!

# Nächsten Termine
- Donnerstag, 18:30 28.April, Grüne Zentrale






