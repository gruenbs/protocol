Sitzungsprotokoll
=====
vom 28.04.16 19:00 - 22:15 Grünen Zentrale, Gartenhaus


# Anwesenheit
## Anwesende Personen
- Thomas Teubel
- Tim Teubel (entschuldigt verspätet)
- Christine Gruber
- Raimund Buchegger (entschuldigt verfrüht abgereist)
- Alois Leitner

## Abwesende Personen
- Christina Sporn

# Themen

## Anträge
### Angenommene Anträge
- ~100€ für Ausrüstung Flüchtlinge U9 Training
-- Mail an Weiß Günther -> "Pressetext" (Foto der Kids!)
-- Auch auf Homepage stellen!

### Abgelehnte Anträge
- Keine

# Allgemeine Informationen
- Straßenbau/Kanal - Sitzung: nichts Erwähnenswertes zu berichten

# Aufgaben
- Kurzer Pressetext über "Malen mit Flüchtlingen" + Fotos!
- Kurzer Pressetext über "Ausstattung Fußballutensilien" + Fotos!
- Plakate für 22.Mai aufstellen
-- Raimund wartet auf Rückmeldung
- Finalisierung und Veröffentlichung der Steckbriefe
- Sommerveranstaltung/Ferienpass:
-- Thomas: Margit Maier oder Magdalena Waldenberger bzgl. Kräuterwanderung anschreiben
--- Kräuterpädagogik / Spiele / Grillerei 
-- Terminfindung: Muss mit der Gemeinde koordiniert werden!

# Nächsten Termine
- Statt Donnerstag, 18:30 26.Mai, eine Woche später am Donnerstag, 2.Juni 18:30 -> Essen mit Ehemaligen bzw. Engagierten
-- Einzuladende Personen: 
--- Didi (verständigt - Anrufbeantworter)
--- Isa (verständigt)
--- Markus Ortbauer
--- Magdalena Waldenberger (verständigt - Anrufbeantworter)
--- Annemarie Rummersdorfer (verständigt)
--- Chrisi + Evelyn Buchegger
--- WIR (Christine, Christina, Raimund, Thomas, Tim, Lois)
-- Vorschlag: Schönagl oder Aschauer (wetterabhängig!)




