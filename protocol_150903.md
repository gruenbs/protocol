Sitzungsprotokoll
=====
vom 03.09.15 19:00 - 22:00 bei Sissi (am Müllerberg)


# Anwesenheit
## Anwesende Personen
- Raimund Buchegger
- Christine Gruber
- Alois Leitner
- Thomas Teubel
- Tim Teubel

## Abwesende Personen
- Markus Ortbauer
- Christina Sporn

# Themen

## Zeitung
- Endversion fertig
- Druck wird sich verzögern (bis 15.09.)
- Post hat 5 Werktage Zeit für Auslieferung

## Plakate
- TTIP Infoverstantaltungsplakate übergeben
- Restliche Wahlauftaktveranstaltungsplakate übergeben

## Hausbesuche
- Wählerverzeichnis nach Straßen aufgeteilt (Raimund, Thomas, Tim, Christine)
- Bestellung Stifte+Stoffsackerl steht noch aus (Tim)

## Homepage / Email
- Zugangsdaten + Einschulung(?) (Lois)
- Aufbau eines zentralen E-Mail-Verteilers (3 Gruppen: Kern, "Gruene", Interessierte)
- Ansprechperson ist Christian Kempter ( +43 680 / 2366997, bezirkgrieskirchen.gruene.at)
- Eigene Sammeladresse für alle ( z.B.: badschallerbach@gruene.at)

## Marketing
- Evaluierung Möglichkeit der Online-Werbung (Lois)
- Motive, Bild-Text-Kombinationen ausarbeiten (alle)
-- Formate z.B. 120x90, 130x60, 180x100, 150x60, etc.
- Raimund als Ansprechperson

# Aufgaben
- Stoffsackerl + Stifte bestellen (Tim)
- Alle noch gültigen E-Mail Adressen an Lois schicken (alle)
- Aktualisieren der Homepage + E-Mail-Verteiler (Lois)
- Hausbesuche nach aufgeteilten Straßen (Raimund, Thomas, Tim, Christine)
- Bild-Text Kombinationen für Onlinewerbung liefern (alle, Deadline?)
- Organisieren eines Infogesprächs bzgl. Online-Marketing mit Raimund eines Sales (Lois)



